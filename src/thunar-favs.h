/*

Filename: thunar-favs.h
Copyright  2005 - 2020  Alexander Nagel
Email: filemanager-avs@acwn.de
Homepage: http://www.acwn.de/projects/filemanager-avs

This file is part of Filemanager-AVS Thunar plugin.

*/

#ifndef __LIBTHUNAR_FILEMANAGER_AVS_H__
#define __LIBTHUNAR_FILEMANAGER_AVS_H__

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/* i18n headers */
#include <glib.h>
#include <libintl.h>
#include <locale.h>
// #include <string.h>

// #include <gmodule.h>
#include <thunarx/thunarx.h>

G_BEGIN_DECLS;

typedef struct _FilemanagerAvsClass FilemanagerAvsClass;
typedef struct _FilemanagerAvs FilemanagerAvs;
 
#define FILEMANAGER_AVS_TYPE            (filemanager_avs_get_type ())
#define FILEMANAGER_AVS(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), FILEMANAGER_AVS_TYPE, FilemanagerAvs))
#define FILEMANAGER_AVS_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  FILEMANAGER_AVS_TYPE, FilemanagerAvsClass))
#define IS_FILEMANAGER_AVS(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), FILEMANAGER_AVS_TYPE))
#define IS_FILEMANAGER_AVS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  FILEMANAGER_AVS_TYPE))
#define FILEMANAGER_AVS_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  FILEMANAGER_AVS_TYPE, FilemanagerAvsClass))

GType filemanager_avs_get_type (void) G_GNUC_CONST;
void  filemanager_avs_register_type (ThunarxProviderPlugin *plugin) G_GNUC_INTERNAL;

G_END_DECLS;

#endif
