/*

Filename: thunar-favs-plugin.c
Copyright  2005 - 2020  Alexander Nagel
Email: filemanager-avs@acwn.de
Homepage: http://www.acwn.de/projects/filemanager-avs

This file is part of Filemanager-AVS Thunar plugin.

*/

/* Headers */
#include "thunar-favs.h"

static GType type_list[1];

/* initialize the thunar plugin */ 
G_MODULE_EXPORT void thunar_extension_initialize (ThunarxProviderPlugin *plugin)
{
	const gchar *mismatch;

	#ifdef ENABLE_NLS
	setlocale (LC_ALL, "");
	bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
	#endif

	/* verify the versions */
	mismatch = thunarx_check_version (THUNARX_MAJOR_VERSION, THUNARX_MINOR_VERSION, THUNARX_MICRO_VERSION);
	if (G_UNLIKELY (mismatch != NULL))
	{
		#ifdef G_ENABLE_DEBUG
			g_warning (gettext ("The version doesn't match: %s"), mismatch);
		#endif
		return;
	}
	#ifdef G_ENABLE_DEBUG
		g_message (gettext ("Register Filemanager-AVS plugin"));
	#endif
	filemanager_avs_register_type (plugin);
	type_list[0] = FILEMANAGER_AVS_TYPE;
}

/* shutdown the thunar plugin */
G_MODULE_EXPORT void thunar_extension_shutdown (void)
{
	#ifdef G_ENABLE_DEBUG
		g_message (gettext ("Shutting down Filemanager-AVS plugin"));
	#endif
}

/* export array of types */ 
G_MODULE_EXPORT void thunar_extension_list_types (const GType **types, gint *n_types)
{
	*types = type_list;
	*n_types = G_N_ELEMENTS (type_list);
}
